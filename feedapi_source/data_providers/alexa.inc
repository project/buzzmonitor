<?php

class AlexaDataProvider implements FeedSourceDataProvider {
  public $field_data_url = 'field_alexa_data_url';
  public $field_rank = 'field_alexa_rank';
  public $field_last_update = 'field_alexa_last_update';

  private $SERVER = 'http://awis.amazonaws.com';
  private $RESPONSE_GROUP = 'Rank';
  private $VERSION = '2005-07-11';

  private $source;
  private $key;
  private $secretKey;
  private $url;
  private $results;
  private $success = false;


  public function setSource(&$source) {
    $this->source = $source;
  }

  public function getSource() {
    return $this->source;
  }

  public function attachData(&$source_node) {
  
      $ALEXA_EXPIRATION_SECONDS = 86400;  // Alexa data can be cached for 24 hours
      $aws_key = variable_get('feedapi_source_aws_access_key_id', '');
      $aws_secret_key = variable_get('feedapi_source_aws_secret_access_key', '');
    
      if ($aws_key == '' || $aws_secret_key == '') {
        watchdog('feedapi_source', 'The AWS Access Key ID and Secret Access Key must be set to access the Alexa web service', array(), WATCHDOG_ERROR);
        return false;
      }
    
      $last_update = $source_node->{ALEXA_LAST_UPDATE_FIELD}[0]['value'];
      $last_update_ts = strtotime($last_update);
      if ($last_update_ts !== FALSE && $last_update_ts > 0 && ($last_update_ts + $ALEXA_EXPIRATION_SECONDS > time())) {
        return false;
      }
    
      $this->setKey($aws_key);
      $this->setSecretKey($aws_secret_key);
      $this->setSource($source_node);
      $this->setUrl($source_node->{SOURCE_URL_FIELD}[0]['value']);
      $this->retrieveData();
      $this->setSourceFields();    
  }

  public function setKey($key) {
    $this->key = $key;
  }

  public function setSecretKey($key) {
    $this->secretKey = $key;
  }

  public function setUrl($url) {
    $this->url = $url;
  }

  public function retrieveData() {
    $params = array();
    $params['Url'] = $this->url;
    $params['ResponseGroup'] = $this->RESPONSE_GROUP;
    $params['Action'] = 'UrlInfo';
    $params['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    $params['Version'] = $this->VERSION;
    $params['AWSAccessKeyId'] = $this->key;
    $params['Signature'] = $this->calculate_RFC2104HMAC($params['Action'] . $params['Timestamp'],$this->secretKey);
    $xmlstr = $this->drupal_geturl($url, $params);
    $xmlstr = preg_replace("/<(\/?)aws:/","<$1",$xmlstr);
    $this->results = simplexml_load_string($xmlstr);
    $trafficData = $this->results->Response->UrlInfoResult->Alexa->TrafficData;
    if ($trafficData !== NULL) {
      $this->dataUrl = (string)$trafficData->DataUrl;
      $this->rank = (int)$trafficData->Rank;
      $this->success = true;
    }
  }

  public function setSourceFields() {
    if ($this->success) {
      $this->source->{$this->field_data_url}[0]['value'] = $this->dataUrl;
      $this->source->{$this->field_rank}[0]['value'] = $this->rank;
      $this->source->{$this->field_last_update}[0]['value'] = date('Y-m-d H:i:s');
    }
  }
  
  public function cron() {}

  private function calculate_RFC2104HMAC($data, $key) { 
    return base64_encode (
      pack("H*", sha1((str_pad($key, 64, chr(0x00))
      ^(str_repeat(chr(0x5c), 64))) .
      pack("H*", sha1((str_pad($key, 64, chr(0x00))
      ^(str_repeat(chr(0x36), 64))) . $data))))
    );
  }
                        
  private function drupal_geturl($url, $params)
  {
    $params['Timestamp'] = urlencode($params['Timestamp']);
    $params['Url'] = urlencode($params['Url']);
    $params['Signature'] = urlencode($params['Signature']);
    $result = drupal_http_request($this->SERVER . '?' . $this->build_query_string($params));
    return $result->data;
  }

  private function build_query_string($params)
  {
    $query = '';
    foreach ($params as $key => $value)
      $query .= $key . '=' . $value . '&';
    return substr($query, 0, strlen($query) - 1);
  }

}
