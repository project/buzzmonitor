<?php

/** It's called "zz..." because we want to make sure this runs after
* Alexa, Compete and Technorati. Abstraction comes with a pricetag.
*
**/
class ZzcombinedDataProvider implements FeedSourceDataProvider {

  public function attachData(&$source_node) {
      $alexaRank = $source_node->{ALEXA_RANK_FIELD}[0]['value'];
      $technoratiAuth = $source_node->{TECHNORATI_AUTHORITY_FIELD}[0]['value']; 
    
    if ($alexaRank == 0 || $alexaRank == NULL)
      $alexaRank = MAX_ALEXA_RANK;
    if ($technoratiAuth == NULL)
      $technoratiAuth = 0;
    
    $alexaWeight = variable_get('feedapi_source_alexa_rank_weight', 1);
    $technoratiWeight = variable_get('feedapi_source_technorati_rank_weight', 1);
  
    $alexaScore = (MAX_ALEXA_RANK - $alexaRank) / MAX_ALEXA_RANK * $alexaWeight;
    $technoratiScore = $technoratiAuth / MAX_TECHNORATI_AUTHORITY * $technoratiWeight;
  
    if ($alexaScore < 0)
      $alexaScore = 0;
    if ($technoratiScore < 0)
      $technoratiScore = 0;
   
    $source_node->{COMBINED_RANK_FIELD}[0]['value'] = $alexaScore + $technoratiScore;
  }
  
  public function cron() {}

}
