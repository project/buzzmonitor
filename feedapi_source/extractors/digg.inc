<?php

class ExtractorDigg implements ISourceExtractor {
  
  var $url;
  
/*
    API DOMAIN: 
    
    
    BY TITLE:
    GET /story/{story clean title}
    http://apidoc.digg.com/ListStories#ExampleRequest
    
    BY URL:    
    GET /stories?link={url_encoded url}
    
    Or By md5 Hash of the URL:
    Get /stories?linkhash=
    http://apidoc.digg.com/ListStories#Arguments
*/
  
  function extract() {
    
    $appkey = urlencode('http://drupal.org/project/buzzmonitor');
    
    //example: "http://digg.com/environment/Leaked_Report_Biofuels_Are_the_Cause_of_Global_Food_Crisis"
    $pattern = '/.*http.?:\/\/.*\/(.*)/is';
    $clean_title = preg_replace($pattern, '$1', $this->url);
    if( strlen($clean_title) == strlen($this->url) ) { //nothing matched
      return false;
    }
    
    $url = "http://services.digg.com/story/" . $clean_title;
    $url .= "?appkey=" . $appkey;
    
    $ret = drupal_http_request($url);
    
    if ( $ret->code != 200 ) return false;
    
    $xml = simplexml_load_string($ret->data, NULL);
    $link = (string)$xml->story->attributes()->link;
    return $link;

  }

}