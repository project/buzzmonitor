<?php

/**
* This is a trivial implementation returning original URL
**/
class ExtractorDefault implements ISourceExtractor {
  
  var $url;
  
  /**
  * This is a trivial implementation returning original URL
  **/
  function extract() {
    return $this->url;
  }
}