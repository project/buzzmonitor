<?php

interface ISourceExtractor {

  public function extract();

}